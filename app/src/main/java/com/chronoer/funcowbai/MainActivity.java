package com.chronoer.funcowbai;

import android.app.Activity;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {

    private final static int RANDOM_NUMBER = 7;
    private final static int MIN = 0;
    private final static int MAX = RANDOM_NUMBER - 1;
    private final static long RANDON_DELAY = 50;//ms

    private TextView mTextView;
    private TextView mMarqueeTextView;
    private Button mButton;

    private Handler mHandler;

    private boolean isRandoming;
    private int mCurIndex = 0;

    private MediaPlayer mMediaPlayer;
    private final static int[] AUDIO_SOUNDS = new int[]{
            R.raw.cowbai1,
            R.raw.cowbai2,
            R.raw.cowbai3,
            R.raw.cowbai4,
            R.raw.cowbai5,
            R.raw.cowbai6,
            R.raw.cowbai7};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();
    }

    private void setView(){
        mTextView = (TextView) this.findViewById(R.id.activity_main_text);
        mMarqueeTextView = (TextView) this.findViewById(R.id.activity_main_marquee);
        mButton = (Button) this.findViewById(R.id.activity_main_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isRandoming){
                    stop();
                }else{
                    start();
                }

            }
        });
    }

    private void start(){
        loopRandom();
        stopSound();
    }

    private void stop(){
        stopRandom();
        updateMarqueeText();
        playSound();
    }

    private void updateText(int color){
        mTextView.setText(String.valueOf(mCurIndex+1));
        mTextView.setTextColor(color);

        mMarqueeTextView.setText("");
    }

    private void updateMarqueeText(){
        mMarqueeTextView.setText(getResources().getStringArray(R.array.cowbai_text_array)[mCurIndex]);
        mMarqueeTextView.setTextColor(mTextView.getCurrentTextColor());
    }

    private void playSound(){
        int soundRes = AUDIO_SOUNDS[mCurIndex];

        mMediaPlayer = MediaPlayer.create(this, soundRes);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setLooping(true);

        mMediaPlayer.start();
    }

    private void stopSound(){
        if(mMediaPlayer != null){
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void loopRandom(){
        if(mHandler == null) {
            mHandler = new Handler();
        }
        isRandoming = true;
        mHandler.postDelayed(mRunnable, RANDON_DELAY);
    }

    private void stopRandom(){
        if(mHandler != null){
            mHandler.removeCallbacks(mRunnable);
            mHandler = null;
        }
        isRandoming = false;
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            Random r = new Random();
            mCurIndex = r.nextInt(MAX - MIN + 1) + MIN;

            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            updateText(color);

            loopRandom();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopSound();
    }
}
